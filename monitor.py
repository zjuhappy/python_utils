"""
reference: 
    [一个pyqt做的网速监控](https://github.com/huawen0327/NetSpeedMonitor)
    [Python查看GPU已使用的显存](https://blog.csdn.net/u011094454/article/details/80774495)
    [清屏](https://www.pythonheidong.com/blog/article/333974/9d94d9c632d8ca9089be/)
"""
import sys
import os
import psutil
import time
from typing import List
import argparse


def get_gpu_info(gpu_id: int):
    """get gpu_used, gpu_total

    Args:
        gpu_id (int): gpu id

    Returns:
        List[int]: [gpu_mem_used, gpu_mem_total]
    """
    try:
        import pynvml

        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(gpu_id)
        meminfo = pynvml.nvmlDeviceGetMemoryInfo(handle)
        return meminfo.used, meminfo.total
    except Exception as e:
        return [0, 0]


def get_network_speed(dt: float):
    """get average net speed between dt(unit: s) time

    Args:
        dt (float): time, unit: second

    Returns:
        tuple(float, float): download_speed, upload_speed
    """
    old_bytes_recv = psutil.net_io_counters().bytes_recv
    old_bytes_sent = psutil.net_io_counters().bytes_sent
    time.sleep(dt)
    new_bytes_recv = psutil.net_io_counters().bytes_recv
    new_bytes_sent = psutil.net_io_counters().bytes_sent
    download_speed = (new_bytes_recv - old_bytes_recv) / dt
    upload_speed = (new_bytes_sent - old_bytes_sent) / dt
    return download_speed, upload_speed


def monitor(file: str = None, gpu_ids: List[int] = [], dt=1):
    """log machine info in csv file

    Args:
        file (str): log csv file path
        gpu_ids (List[int]): gpu indexes
        dt (int, optional): log interval time second. Defaults to 10.
    """

    try:
        if file:
            assert file.endswith(".csv")
            fp = open(file, "w")
            # csv file header
            print(
                "down, up, mem_used, mem_total, "
                + ", ".join(["gpu" + str(gpu_id) + "_used" for gpu_id in gpu_ids]),
                file=fp,
                flush=True,
            )
        while True:
            download_speed, upload_speed = get_network_speed(dt=dt)
            mem_used = psutil.virtual_memory().used
            mem_total = psutil.virtual_memory().total

            if file:
                # save to file
                print(
                    "%d, %d, %d, %d, "
                    % (download_speed, upload_speed, mem_used, mem_total)
                    + ", ".join([str(get_gpu_info(gpu_id)[0]) for gpu_id in gpu_ids]),
                    file=fp,
                    flush=True,
                )

            # show in terminal
            os.system("cls" if os.name == "nt" else "clear")
            print("down: %.2f kB/s" % (download_speed / 1024))
            print("up  : %.2f kB/s" % (upload_speed / 1024))
            print(
                "mem : %.2f GB/%.2f GB " % (mem_used / (2 ** 30), mem_total / (2 ** 30))
            )
            for gpu_id in gpu_ids:
                gpu_used, gpu_total = get_gpu_info(gpu_id)
                print(
                    "gpu%d: %.2f GB/%.2f GB "
                    % (gpu_id, gpu_used / (2 ** 30), gpu_total / (2 ** 30))
                )
    finally:
        if file:
            fp.close()


if __name__ == "__main__":
    # sys.argv = [
    #     x
    #     for x in [sys.argv[0]] + "--gpus 0 1 --file 123.csv --dt 2".split(" ")
    #     if len(x) > 0
    # ]
    parser = argparse.ArgumentParser(description="show/save system resource used")
    parser.add_argument("--file", type=str, default=None, help="file to store history")
    parser.add_argument("--gpus", type=int, nargs="+", default=[], help="gpu indexes")
    parser.add_argument("--dt", type=int, default=1, help="refresh interval")
    args = parser.parse_args()

    monitor(args.file, args.gpus, args.dt)
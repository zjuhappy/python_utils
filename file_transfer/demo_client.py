import os

from file_transfer import FileClient

if __name__=="__main__":
    client = FileClient('127.0.0.1:28888')
    # client.upload("LICENSE43", "./123/LICENSE2")
    client.download("/home/hp/Documents/docker_share/etc/Anaconda3-5.2.0-Linux-x86_64.sh", "Anaconda3-5.2.0-Linux-x86_64.sh")
    
    # server_dir = '.'
    # file_name_list = client.listdir(server_dir)
    # for file_name in file_name_list:
    #     server_path = os.path.join(server_dir, file_name).replace("\\", "/")
    #     if client.isdir(server_path):
    #         continue
    #     client.download(server_path, os.path.join("temp", file_name))
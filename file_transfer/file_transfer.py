import os
from concurrent import futures
import grpc
import time
import logging

import chunk_pb2, chunk_pb2_grpc

CHUNK_SIZE = 1024 * 1024  # 1MB

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("[%(asctime)s %(filename)s:%(lineno)d] %(message)s")
console_handler = logging.StreamHandler()
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


def get_file_chunks(file_path):
    with open(file_path, "rb") as f:
        while True:
            piece = f.read(CHUNK_SIZE)
            if len(piece) == 0:
                return
            yield chunk_pb2.Chunk(data=piece)


def save_chunks_to_file(chunks, file_path):
    with open(file_path, "wb") as f:
        for chunk in chunks:
            f.write(chunk.data)


class FileClient:
    def __init__(self, address):
        channel = grpc.insecure_channel(address)
        self.stub = chunk_pb2_grpc.FileServerStub(channel)

    def __upload_file(self, client_path, server_path):
        chunks_generator = get_file_chunks(client_path)
        error_code = self.stub.set_upload_path(chunk_pb2.String(data=server_path))
        assert error_code.data == 0
        response = self.stub.upload(chunks_generator)
        assert response.data == os.path.getsize(client_path)

    def __upload_folder(self, client_dir, server_dir):
        file_name_list = os.listdir(client_dir)
        for file_name in file_name_list:
            server_path = os.path.join(server_dir, file_name).replace("\\", "/")
            client_path = os.path.join(client_dir, file_name)
            if os.path.isdir(client_path):
                self.__upload_folder(client_path, server_path)
            else:
                self.__upload_file(client_path, server_path)

    def upload(self, client_path, server_path):
        if os.path.isdir(client_path):
            self.__upload_folder(client_path, server_path)
        else:
            self.__upload_file(client_path, server_path)

    def __download_file(self, server_path, client_path):
        _dir = os.path.split(client_path)[0]
        if len(_dir) > 0:
            os.makedirs(_dir, exist_ok=True)
        response = self.stub.download(chunk_pb2.String(data=server_path))
        save_chunks_to_file(response, client_path)

    def __download_folder(self, server_dir, client_dir):
        file_name_list = self.listdir(server_dir)
        for file_name in file_name_list:
            server_path = os.path.join(server_dir, file_name).replace("\\", "/")
            client_path = os.path.join(client_dir, file_name)
            if self.isdir(server_path):
                self.__download_folder(server_path, client_path)
            else:
                self.__download_file(server_path, client_path)

    def download(self, server_path, client_path):
        if self.isdir(server_path):
            self.__download_folder(server_path, client_path)
        else:
            self.__download_file(server_path, client_path)

    def listdir(self, dir_):
        return [item.data for item in self.stub.listdir(chunk_pb2.String(data=dir_))]

    def isdir(self, path):
        ans = False
        if (self.stub.isdir(chunk_pb2.String(data=path))).data == 1:
            ans = True
        return ans


class Servicer(chunk_pb2_grpc.FileServerServicer):
    def __init__(self):
        self.upload_path = ""

    def upload(self, request_iterator, context):
        save_chunks_to_file(request_iterator, self.upload_path)
        return chunk_pb2.Int64(data=os.path.getsize(self.upload_path))

    def download(self, request, context):
        file_path = request.data
        return get_file_chunks(file_path)

    def set_upload_path(self, request, context):
        self.upload_path = request.data
        _dir = os.path.split(self.upload_path)[0]
        if len(_dir) > 0:
            os.makedirs(_dir, exist_ok=True)
        return chunk_pb2.Int64(data=0)

    def listdir(self, request, context):
        file_path = request.data
        for file_name in os.listdir(file_path):
            yield chunk_pb2.String(data=file_name)

    def isdir(self, request, context):
        file_path = request.data
        ans = 0
        if os.path.isdir(file_path):
            ans = 1
        return chunk_pb2.Int64(data=ans)


class FileServer(chunk_pb2_grpc.FileServerServicer):
    def __init__(self):
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=255))
        chunk_pb2_grpc.add_FileServerServicer_to_server(Servicer(), self.server)

    def start(self, port):
        self.server.add_insecure_port(f"[::]:{port}")
        self.server.start()
        logger.debug("file server is running at port %d" % (port))

        try:
            while True:
                time.sleep(60 * 60 * 24)
        except KeyboardInterrupt:
            self.server.stop(0)

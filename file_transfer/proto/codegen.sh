# for js
protoc -I=. *.proto --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:. 
# for python
protoc  -I. --python_out=. --grpc_python_out=. *.proto
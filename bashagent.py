import pexpect
import time
from threading import Thread
import json
import argparse
import logging
import signal


logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "[%(asctime)s %(filename)s:%(lineno)d PID:%(process)d TID:%(thread)d] %(message)s"
)

for handler in [
    # logging.StreamHandler(),
    logging.FileHandler("/tmp/bashagent.log", mode="a"),
]:
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def stop_thread(thread):
    import ctypes
    import inspect

    def _async_raise(tid, exctype):
        """raises the exception, performs cleanup if needed"""
        tid = ctypes.c_long(tid)
        if not inspect.isclass(exctype):
            exctype = type(exctype)
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, ctypes.py_object(exctype))
        if res == 0:
            pass
            # raise ValueError("invalid thread id")
        elif res != 1:
            # """if it returns a number greater than one, you're in trouble,
            # and you should call it again with exc=NULL to revert the effect"""
            ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
            raise SystemError("PyThreadState_SetAsyncExc failed")

    _async_raise(thread.ident, SystemExit)


class BashAgent:
    def __init__(self, script="", cool_time=0.01):
        self.__cool_time = cool_time
        self.__threads = []
        for keyword, original in {
            "^B": "\x02",
            "^CLEAR": "\x7f" * 10,
        }.items():
            script = script.replace(keyword, original)
        self.script_agent, self.script_exit = script.split("Exit:")
        for bad_keyword in ["expect", "Agent"]:
            assert bad_keyword not in self.script_exit, logger.debug(
                f"{bad_keyword} in Exit block is not allowed"
            )

    def __call__(self):
        def __exit(*args):
            logger.debug("exit in " + str(args))
            for thread in self.__threads:
                stop_thread(thread)
            self.__run(self.script_exit)
            logger.debug("exit out ")

        try:
            logger.debug("start")
            for cmds in self.script_agent.split("Agent:"):
                self.__run(cmds)
            signal.signal(signal.SIGTERM, __exit)
            for thread in self.__threads:
                thread.join()
        except Exception as e:
            logger.debug(e)
        finally:
            __exit()

    def __run(self, cmds: str):
        agent = pexpect.spawn("bash")
        agent.setwinsize(100, 300)
        for cmd in cmds.split("\n"):
            cmd = cmd.strip()
            if len(cmd) < 1 or cmd.startswith("#"):
                continue
            self.___run(agent, cmd)

    def __parse_key_args(self, arg_str: str):
        key_args = dict()
        for item in arg_str.split(","):
            item = item.strip()
            if len(item) < 1:
                continue
            key, value = item.split(":")
            key_args[key] = value

        return key_args

    def __sendline(self, agent, cmd):
        if cmd.startswith("sleep"):
            time.sleep(float(cmd[5:]))
        else:
            agent.sendline(cmd)
            time.sleep(self.__cool_time)

    def __expect(self, agent, pattern_answer_dict):
        exception_list = [pexpect.EOF, pexpect.TIMEOUT]
        patterns = exception_list + list(pattern_answer_dict.keys())
        index = agent.expect(patterns, timeout=3600)
        if index < len(exception_list):
            return
        time.sleep(self.__cool_time)
        pattern = patterns[index]
        self.__sendline(agent, pattern_answer_dict[pattern])

    def ___run(self, agent, cmd: str):
        if cmd.startswith("expect "):
            pattern_answer_dict = self.__parse_key_args(cmd.replace("expect ", ""))
            self.__expect(agent, pattern_answer_dict)
        elif cmd.startswith("loop_expect "):
            pattern_answer_dict = self.__parse_key_args(cmd.replace("loop_expect ", ""))

            def loop_expect():
                while True:
                    self.__expect(agent, pattern_answer_dict)

            thread = Thread(target=loop_expect, args=())
            thread.start()
            self.__threads.append(thread)
        else:
            self.__sendline(agent, cmd)


def exemple():
    buf = """
    Agent:
    tmux kill-session -t tunnle
    tmux new -s tunnle

    ^Bc
    ^B,
    ^CLEARtunnle_ssh_1
    unset TMUX
    tmux kill-session -t __tunnle_ssh_1
    tmux new -s __tunnle_ssh_1

    ^Bc    
    ^B,
    ^CLEARtunnle_ssh_2
    unset TMUX
    tmux kill-session -t __tunnle_ssh_2
    tmux new -s __tunnle_ssh_2

    ^Bc
    ^B,
    ^CLEARtunnle_ssh_3
    unset TMUX
    tmux kill-session -t __tunnle_ssh_3
    tmux new -s __tunnle_ssh_3

    Agent:
    tmux a -t __tunnle_ssh_1
    autossh -R 24001:127.0.0.1:22 tunnle@ishappy.top
    loop_expect password:123,yes:yes

    Agent:
    tmux a -t __tunnle_ssh_2
    autossh -R 24002:127.0.0.1:22 tunnle@ishappy.top
    loop_expect password:123,yes:yes

    Agent:
    tmux a -t __tunnle_ssh_3
    autossh -R 24003:127.0.0.1:22 tunnle@ishappy.top
    loop_expect password:123,yes:yes
    """
    BashAgent(buf)()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Execute BashAgent script.")
    parser.add_argument(
        "-f",
        "--file",
        type=str,
        default=None,
        help="BashAgent script path",
    )
    parser.add_argument(
        "--script",
        type=str,
        default=None,
        help="BashAgent script",
    )
    parser.add_argument(
        "--cool_time",
        type=float,
        default=0.01,
        help="BashAgent script",
    )
    args = parser.parse_args()
    assert (args.file is not None) ^ (args.script is not None)

    if args.file is not None:
        with open(args.file, "r") as fp:
            buf = fp.read()
            BashAgent(buf, cool_time=args.cool_time)()
    elif args.script is not None:
        BashAgent(args.script, cool_time=args.cool_time)()
import datetime
from time import time, strftime
import os
import sys

def watchdog(t_max, callback, *args, **kargs):
    import threading
    import time
    import inspect
    import ctypes

    def _async_raise(tid, exctype):
        """Raises an exception in the threads with id tid"""
        if not inspect.isclass(exctype):
            raise TypeError("Only types can be raised (not instances)")
        res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
            ctypes.c_long(tid), ctypes.py_object(exctype))
        if res == 0:
            raise ValueError("invalid thread id")
        elif res != 1:
            # """if it returns a number greater than one, you're in trouble,
            # and you should call it again with exc=NULL to revert the effect"""
            ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
            raise SystemError("PyThreadState_SetAsyncExc failed")

    def stop_thread(thread):
        _async_raise(thread.ident, SystemExit)

    class _Thread(threading.Thread):
        def run(self):
            try:
                self.exception = None
                self.result = callback(*args, **kargs)
            except Exception as e:
                self.exception = e

    thread = _Thread()
    thread.start()
    cnt = 0
    frequency = 10.
    while thread.is_alive():
        time.sleep(1.0 / frequency)
        cnt = cnt + 1
        if cnt > t_max * frequency:
            stop_thread(thread)
            raise ValueError("time out")

    if thread.exception != None:
        raise ValueError(thread.exception)

    return thread.result


class TimeMeter:
    def __init__(self, is_debug=True):
        self.t_old = time()
        self.is_debug = is_debug

    def __call__(self, msg=""):
        if self.is_debug:
            t_now = time()
            print(
                "[",
                datetime.datetime.now().strftime("%Y/%m/%d/%H:%M:%S.%f"),
                "%s:%d"
                % (
                    os.path.split(sys._getframe().f_back.f_code.co_filename)[-1],
                    sys._getframe().f_back.f_lineno,
                ),
                "PID:%s" % os.getpid(),
                "TIME:%.6f" % (t_now - self.t_old),
                "]",
                msg,
                flush=True,
            )
            self.t_old = t_now



def port2port(port_list, remote_host_port, remote_host_user, remote_host_ip, stdout_file_dir):
    os.makedirs(stdout_file_dir, exist_ok=True)
    process_list = []

    for port in port_list:
        with open(os.path.join(stdout_file_dir, str(port)+".txt"), "w") as outfile:
            process_list.append(subprocess.Popen(
                ("ssh -L *:%d:127.0.0.1:%d -p %d %s@%s" %
                 (port, port, remote_host_port, remote_host_user, remote_host_ip)
                 )
                .split(" "),
                stdout=outfile
            )
            )
        print("port", port)

    print("port2port ok")

    try:
        count = 0
        while True:
            time.sleep(1)
            print("\rport2port is running "+"." *
                  (count+1)+" "*(10-count), end="")
            count = (count+1) % 3
    except KeyboardInterrupt as e:
        print(e)
    finally:
        for p in process_list:
            p.terminate()
        print("port2port finish")



def create_symlink(source_dir, destination_dir):
    import os
    import shutil
    import logging

    logging.basicConfig(
        format="[%(asctime)s %(filename)s:%(lineno)d] %(message)s", level=logging.DEBUG)

    def delete(path):
        if os.path.islink(path):
            os.remove(path)
        elif os.path.isdir(path):
            shutil.rmtree(path)
        elif os.path.isfile(path):
            os.remove(path)

    dst_item_set = set(os.listdir(destination_dir))
    all_yes_flag = False
    all_no_flag = False
    for item_name in os.listdir(source_dir):
        src_path = os.path.abspath(os.path.join(source_dir, item_name))
        dst_path = os.path.abspath(os.path.join(destination_dir, item_name))
        logging.debug("src_path: %s, dst_path: %s" % (src_path, dst_path))
        create_symlink_flag = False
        if item_name in dst_item_set:
            if all_yes_flag:
                delete(dst_path)
                create_symlink_flag = True
            elif all_no_flag:
                pass
            else:
                while True:
                    choose = input("relink %s Y(yes)/N(no)/YA(yes for all)/NA(no for all): " %
                                   (dst_path))

                    if choose == "Y":
                        logging.debug(choose)
                        delete(dst_path)
                        create_symlink_flag = True
                        break
                    elif choose == "N":
                        logging.debug(choose)
                        break
                    elif choose == "YA":
                        logging.debug(choose)
                        delete(dst_path)
                        all_yes_flag = True
                        create_symlink_flag = True
                        break
                    elif choose == "NA":
                        logging.debug(choose)
                        all_no_flag = True
                        break
                    else:
                        logging.debug(choose)
                        print("bad input")
        else:
            create_symlink_flag = True

        if create_symlink_flag:
            logging.debug(create_symlink_flag)
            os.symlink(src_path, dst_path)


def param_dict_generator(param_dicts, repeat=1):
    """ 
    Description: generator of params
    Input: 
        param_dicts: Dict of params, such as {'a':[1, 2], 'b':[3, 4]}
        repeat: number of repeate time
    Output: 
        param_dict generator, because `yield` is used
    Sample: 
        ```
        def func(a, b):
            return a+b
        param_dicts = {"a":[1, 2], "b":[3, 4]}
        df_result = pd.DataFrame({key: [] for key in param_dicts})
        for param_dict in param_dict_generator(param_dicts, repeat = 2):
            result = func(**param_dict)
            df_result = df_result.append({**{key: value for key, value in param_dict.items()}, **{"result": [result]}}, ignore_index=True)
        ```
    """
    for _ in range(repeat):
        for param_values in itertools.product(*param_dicts.values()):
            param_dict = {}
            for key, value in zip(param_dicts.keys(), param_values):
                param_dict[key] = value
            yield param_dict
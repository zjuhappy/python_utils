import subprocess
from multiprocessing import Process
import os
import time


def port2port(remote_host_user, remote_host_ip, remote_host_port, port_list, stdout_file_dir="subprocess_stdout"):
    os.makedirs(stdout_file_dir, exist_ok=True)
    process_list = []

    for port in port_list:
        with open(os.path.join(stdout_file_dir, "%s_%s_%s_%d.txt" % (remote_host_user, remote_host_ip, remote_host_port, port)), "w") as outfile:
            process_list.append(subprocess.Popen(
                ("ssh -L *:%d:127.0.0.1:%d -p %d %s@%s" %
                 (port, port, remote_host_port, remote_host_user, remote_host_ip)
                 )
                .split(" "),
                stdout=outfile
            )
            )

        print("remote_host_user: %s, remote_host_ip: %s, remote_host_port: %s, port: %d" %
              (remote_host_user, remote_host_ip, remote_host_port, port))

    return process_list


if __name__ == "__main__":

    port2port_list = [
        {"remote_host_user": "root", "remote_host_ip": "10.252.2.1",
            "remote_host_port": 10003, "port_list": [10012, 6006]},
        {"remote_host_user": "root", "remote_host_ip": "10.252.2.1",
            "remote_host_port": 22, "port_list": [10011]},
    ]

    process_list = []

    for item in port2port_list:
        remote_host_user = item["remote_host_user"]
        remote_host_ip = item["remote_host_ip"]
        remote_host_port = item["remote_host_port"]
        port_list = item["port_list"]
        process_list += port2port(remote_host_user, remote_host_ip,
                                  remote_host_port, port_list)

    try:
        count = 0
        while True:
            time.sleep(1)
            print("\rport2port is running "+"." *
                  (count+1)+" "*(10-count), end="")
            count = (count+1) % 3
    except KeyboardInterrupt as e:
        print(e)
    finally:
        for p in process_list:
            p.terminate()
        print("port2port finish")
